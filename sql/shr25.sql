SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_sys_area
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_area`;
CREATE TABLE `t_sys_area`  (
  `id` bigint NOT NULL COMMENT '主键',
  `area_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '区代码',
  `city_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父级市代码',
  `area_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '市名称',
  `short_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '简称',
  `lng` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '纬度',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `data_state` bigint NULL DEFAULT NULL COMMENT '状态',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人名称',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  INDEX `Index_1`(`area_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '地区设置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_area
-- ----------------------------
INSERT INTO `t_sys_area` VALUES (1975, '500101', '500100', '万州区', '万州', '108.380249', '30.807808', 28, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1976, '500102', '500100', '涪陵区', '涪陵', '107.394905', '29.703651', 11, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1977, '500103', '500100', '渝中区', '渝中', '106.562881', '29.556742', 37, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1978, '500104', '500100', '大渡口区', '大渡口', '106.48613', '29.481003', 6, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1979, '500105', '500100', '江北区', '江北', '106.532845', '29.575352', 13, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1980, '500106', '500100', '沙坪坝区', '沙坪坝', '106.454201', '29.541224', 24, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1981, '500107', '500100', '九龙坡区', '九龙坡', '106.480988', '29.523493', 15, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1982, '500108', '500100', '南岸区', '南岸', '106.560814', '29.523993', 18, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1983, '500109', '500100', '北碚区', '北碚', '106.437866', '29.82543', 2, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1984, '500112', '500100', '渝北区', '渝北', '106.512848', '29.601452', 35, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1985, '500113', '500100', '巴南区', '巴南', '106.519424', '29.38192', 1, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1986, '500114', '500100', '黔江区', '黔江', '108.782578', '29.527548', 21, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1987, '500115', '500100', '长寿区', '长寿', '107.074852', '29.833672', 4, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1988, '500222', '500100', '綦江区', '綦江', '106.651421', '29.028091', 22, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1989, '500223', '500100', '潼南县', '潼南', '105.84182', '30.189554', 27, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1990, '500224', '500100', '铜梁县', '铜梁', '106.054947', '29.839945', 26, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1991, '500225', '500100', '大足区', '大足', '105.715317', '29.700499', 7, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1992, '500226', '500100', '荣昌县', '荣昌', '105.594063', '29.403627', 23, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1993, '500227', '500100', '璧山县', '璧山', '106.231125', '29.59358', 3, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1994, '500228', '500100', '梁平县', '梁平', '107.800034', '30.672169', 17, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1995, '500229', '500100', '城口县', '城口', '108.664902', '31.946293', 5, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1996, '500230', '500100', '丰都县', '丰都', '107.732483', '29.866425', 9, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1997, '500231', '500100', '垫江县', '垫江', '107.348694', '30.330011', 8, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1998, '500232', '500100', '武隆县', '武隆', '107.756554', '29.323759', 29, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (1999, '500233', '500100', '忠县', '忠县', '108.037521', '30.291536', 38, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (2000, '500234', '500100', '开县', '开县', '108.413315', '31.167734', 16, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (2001, '500235', '500100', '云阳县', '云阳', '108.697701', '30.930529', 36, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (2002, '500236', '500100', '奉节县', '奉节', '109.465775', '31.019966', 10, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (2003, '500237', '500100', '巫山县', '巫山', '109.878929', '31.074842', 30, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (2004, '500238', '500100', '巫溪县', '巫溪', '109.628914', '31.396601', 31, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (2005, '500240', '500100', '石柱土家族自治县', '石柱', '108.11245', '29.998529', 25, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (2006, '500241', '500100', '秀山土家族苗族自治县', '秀山', '108.99604', '28.444773', 32, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (2007, '500242', '500100', '酉阳土家族苗族自治县', '酉阳', '108.767204', '28.839828', 34, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (2008, '500243', '500100', '彭水苗族土家族自治县', '彭水', '108.16655', '29.293856', 20, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (2009, '500381', '500100', '江津区', '江津', '106.253159', '29.283386', 14, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (2010, '500382', '500100', '合川区', '合川', '106.265556', '29.990993', 12, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (2011, '500383', '500100', '永川区', '永川', '105.894714', '29.348747', 33, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);
INSERT INTO `t_sys_area` VALUES (2012, '500384', '500100', '南川区', '南川', '107.098152', '29.156647', 19, 0, 1, 'admin', '2022-03-09 22:01:51', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_sys_city
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_city`;
CREATE TABLE `t_sys_city`  (
  `id` bigint NOT NULL COMMENT '主键',
  `city_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '市代码',
  `city_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '市名称',
  `short_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '简称',
  `province_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省代码',
  `lng` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '纬度',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `data_state` int NULL DEFAULT NULL COMMENT '状态',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `Index_1`(`city_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '城市设置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_city
-- ----------------------------
INSERT INTO `t_sys_city` VALUES (255, '500100', '重庆市', '重庆', '500000', '106.504959', '29.533155', 1, 0, NULL, '', '2019-02-28 17:16:58', NULL, '', '2019-02-28 17:17:05');

-- ----------------------------
-- Table structure for t_sys_department
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_department`;
CREATE TABLE `t_sys_department`  (
  `id` bigint NOT NULL COMMENT '主键',
  `parent_id` bigint NULL DEFAULT NULL COMMENT '父id',
  `dept_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `leader` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门负责人',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` int NULL DEFAULT NULL COMMENT '状态',
  `order_num` int NULL DEFAULT NULL COMMENT '排序',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_department
-- ----------------------------
INSERT INTO `t_sys_department` VALUES (1, 0, '树人助人', 'admin', '13012345678', 'v2@qq.com', 1, 1, 1, 'admin', '2022-03-09 22:12:28', 1, '管理员', '2022-03-13 22:28:27');

-- ----------------------------
-- Table structure for t_sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_dict_data`;
CREATE TABLE `t_sys_dict_data`  (
  `id` bigint NOT NULL COMMENT '主键',
  `dict_sort` int NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_dict_data
-- ----------------------------
INSERT INTO `t_sys_dict_data` VALUES (331043380933038080, 1, '一般', '1', 'sys_notice_type', '', 'info', 'Y', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (331043525137403904, 2, '重要', '2', 'sys_notice_type', '', 'important', 'N', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (340080322395901952, 1, '开启', '0', 'sys_province_state', '', 'info', 'Y', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (340080779201744896, 2, '关闭', '-1', 'sys_province_state', '', 'important', 'N', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (373494384659927040, 0, 'GET请求', '1', 'sys_inter_url_type', '', 'primary', 'Y', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (373494483465146368, 1, 'POST请求', '2', 'sys_inter_url_type', '', 'info', 'N', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (506431838588375040, 0, 'DELETE请求', '3', 'sys_inter_url_type', '', 'default', 'N', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (506432620712824832, 0, 'PUT请求', '4', 'sys_inter_url_type', '', 'default', 'N', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (563746747239763968, 0, '微信', '1', 'payment_type', '', 'default', 'Y', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (563746783184949248, 0, '支付宝', '2', 'payment_type', '', 'default', 'Y', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (563746818496794624, 0, '水滴筹', '3', 'payment_type', '', 'default', 'Y', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (571366029360500736, 0, '是', '1', 'yes_or_no', '', 'default', 'Y', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (571366105029939200, 0, '否', '-1', 'yes_or_no', '', 'info', 'Y', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (1501489308477526017, 1, '未提交', '0', 'task_status', '', 'default', 'Y', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (1501489416971587586, 2, '提交', '1', 'task_status', '', 'default', 'Y', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (1501489519283245058, 3, '确认', '2', 'task_status', '', 'default', 'Y', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (1501489584173322242, 4, '驳回', '3', 'task_status', '', 'default', 'Y', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (1501489635813593089, 5, '撤回', '4', 'task_status', '', 'default', 'Y', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (1501489672438255618, 6, '发布', '5', 'task_status', '', 'default', 'Y', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_data` VALUES (1501489720177823745, 7, '结束', '6', 'task_status', '', 'default', 'Y', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_dict_type`;
CREATE TABLE `t_sys_dict_type`  (
  `id` bigint NOT NULL COMMENT '主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_dict_type
-- ----------------------------
INSERT INTO `t_sys_dict_type` VALUES (6, '通知类型', 'sys_notice_type', '0', '通知类型列表', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_type` VALUES (340079827459641344, '省份状态', 'sys_province_state', '0', '省份状态', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_type` VALUES (373493952487231488, '拦截器类型', 'sys_inter_url_type', '0', '拦截器类型', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_type` VALUES (563746635880992768, '捐款类型', 'payment_type', '0', '', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_type` VALUES (571365854613213184, '是与否', 'yes_or_no', '0', '用于select', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_type` VALUES (1501482264001323009, '任务状态', 'task_status', '0', '任务状态', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_sys_email
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_email`;
CREATE TABLE `t_sys_email`  (
  `id` bigint NOT NULL COMMENT '主键',
  `receivers_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '接收人电子邮件',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '邮件标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT '内容',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '电子邮件' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_email
-- ----------------------------
INSERT INTO `t_sys_email` VALUES (503928650819833856, '87766867@qq.com', '87766867@qq.com', 'fffffff<img src=\"http://localhost:8080/demo/static/component/layui/images/face/22.gif\" alt=\"[委屈]\">', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_email` VALUES (503928914918379520, '87766867@qq.com', '87766867@qq.com', 'ssssssfsdfsdfsdf<img src=\"http://localhost:8080/demo/static/component/layui/images/face/42.gif\" alt=\"[抓狂]\"><img src=\"http://localhost:8080/demo/static/component/layui/images/face/71.gif\" alt=\"[蛋糕]\">', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_email` VALUES (595001021625794560, '87766867@qq.com', 'springbootv2测试邮件', '<p>测试测测测</p>', 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_sys_file
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_file`;
CREATE TABLE `t_sys_file`  (
  `id` bigint NOT NULL COMMENT '主键',
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '文件名字',
  `bucket_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '桶名',
  `file_size` bigint NULL DEFAULT NULL COMMENT '文件大小',
  `file_suffix` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '后缀',
  `file_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '本地路径',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '文件信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_file
-- ----------------------------

-- ----------------------------
-- Table structure for t_sys_inter_url
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_inter_url`;
CREATE TABLE `t_sys_inter_url`  (
  `id` bigint NOT NULL COMMENT '主键',
  `inter_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拦截名称',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拦截url',
  `type` bigint NULL DEFAULT NULL COMMENT '类型',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '拦截url表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_inter_url
-- ----------------------------
INSERT INTO `t_sys_inter_url` VALUES (411495038321823744, '字典表新增', '/DictDataController/add', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506433268967673856, '字典表修改', '/DictDataController/edit', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506434978159136768, '字典表删除', '/DictDataController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506435565655298048, '字典表状态修改', '/DictDataController/updateDefault', 4, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506435921147727872, '字典表状态修改2', '/DictDataController/updateEnable', 4, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506436031403397120, '字典表类型新增', '/DictTypeController/add', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506436148680331264, '字典表类型修改', '/DictTypeController/edit', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506436165776314368, '字典表类型删除', '/DictTypeController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506436180578013184, '字典表类型状态修改', '/DictTypeController/updateEnable', 4, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506436662134444032, '邮件新增', '/EmailController/add', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506436757722632192, '邮件删除', '/EmailController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506437010966319104, '日志删除', '/LogController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506437420099702784, 'oss新增', '/oss/bucket/', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506437439112482816, 'oss删除', '/oss/bucket/', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506437964436475904, '权限新增', '/PermissionController/add', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506438040823140352, '权限保存', '/PermissionController/edit', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506438121399914496, '权限删除', '/PermissionController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506438208599494656, '权限授权', '/PermissionController/saveRolePower', 4, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506438306276446208, '权限状态修改', '/PermissionController/updateVisible', 4, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506438447226032128, '定时器新增', '/SysQuartzJobController/add', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506438589874311168, ' 任务调度状态修改', '/SysQuartzJobController/changeStatus', 4, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506438725388079104, '定时器保存', '/SysQuartzJobController/edit', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506438870959788032, '定时器修改', '/SysQuartzJobController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506439003516571648, '定时任务日志删除', '/SysQuartzJobLogController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506439171481669632, '角色新增', '/RoleController/add', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506439186778296320, '角色修改', '/RoleController/edit', 4, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506439297122045952, '角色删除', '/RoleController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506439669773373440, '地区新增', '/SysAreaController/add', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506439687859212288, '地区修改', '/SysAreaController/edit', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506439835490324480, '地区删除', '/SysAreaController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506440103976112128, 'City新增', '/SysCityController/add', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506440145147400192, 'City修改', ' /SysCityController/edit', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506440217188765696, 'City删除', '/SysCityController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506440386873528320, '部门新增', '/SysDepartmentController/add', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506440448223612928, '部门修改', '/SysDepartmentController/edit', 4, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506440515110178816, '部门删除', '/SysDepartmentController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506440574635741184, '部门状态', '/SysDepartmentController/updateVisible', 4, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506440668508459008, '拦截器url新增', '/SysInterUrlController/add', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506440708056551424, '拦截器url修改', '/SysInterUrlController/edit', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506440802856210432, '拦截器url删除', '/SysInterUrlController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506441001783660544, '公告新增', '/SysNoticeController/add', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506441051263864832, '公告修改', '/SysNoticeController/edit', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506441105743679488, '公告删除', '/SysNoticeController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506441242591236096, '职位新增', '/SysPositionController/add', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506441287038275584, '职位修改', '/SysPositionController/edit', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506441350200299520, '职位删除', '/SysPositionController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506441420677189632, '职位状态修改', '/SysPositionController/updateVisible', 4, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506441780003213312, '省份新增', '/SysProvinceController/add', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506441807383629824, '省份修改', '/SysProvinceController/edit', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506441871850082304, '省份删除', '/SysProvinceController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506441980012793856, '街道新增', '/SysStreetController/add', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506442015706320896, '街道修改', '/SysStreetController/edit', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506442092445306880, '街道删除', '/SysStreetController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506442186552905728, '用户新增', '/UserController/add', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506442212696002560, '用户修改', '/UserController/edit', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506442271252680704, '用户修改密码', '/UserController/editPwd', 2, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506442344443285504, '用户删除', '/UserController/remove', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_inter_url` VALUES (506444610625736704, '拦截器url复制', '/SysInterUrlController/copy/', 3, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_notice`;
CREATE TABLE `t_sys_notice`  (
  `id` bigint NOT NULL COMMENT '主键',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT '内容',
  `type` int NULL DEFAULT NULL COMMENT '类型',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '公告' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_notice
-- ----------------------------

-- ----------------------------
-- Table structure for t_sys_notice_user
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_notice_user`;
CREATE TABLE `t_sys_notice_user`  (
  `id` bigint NOT NULL COMMENT '主键',
  `notice_id` bigint NULL DEFAULT NULL COMMENT '公告id',
  `user_id` bigint NULL DEFAULT NULL COMMENT '用户id',
  `state` bigint NULL DEFAULT NULL COMMENT '0未阅读 1 阅读',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '公告_用户外键' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_notice_user
-- ----------------------------

-- ----------------------------
-- Table structure for t_sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_oper_log`;
CREATE TABLE `t_sys_oper_log`  (
  `id` bigint NOT NULL COMMENT '主键',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '标题',
  `method` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '方法',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'url',
  `oper_param` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '参数',
  `error_msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '日志记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_oper_log
-- ----------------------------

-- ----------------------------
-- Table structure for t_sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_permission`;
CREATE TABLE `t_sys_permission`  (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限名称',
  `descripion` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限描述',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权链接',
  `is_blank` int NULL DEFAULT 0 COMMENT '是否跳转 0 不跳转 1跳转',
  `pid` bigint NULL DEFAULT NULL COMMENT '父节点id',
  `perms` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `type` int NULL DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `order_num` int NULL DEFAULT NULL COMMENT '排序',
  `visible` int NULL DEFAULT NULL COMMENT '是否可见',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_permission
-- ----------------------------
INSERT INTO `t_sys_permission` VALUES (4, '用户管理', '用户展示', '/UserController/view', 0, 411522822607867904, 'system:user:view', 1, 'icon icon-user', 1, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (5, '用户集合', '用户集合', '/UserController/list', 0, 4, 'system:user:list', 2, '', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (6, '用户添加', '用户添加', '/UserController/add', 0, 4, 'system:user:add', 2, 'entypo-plus-squared', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (7, '用户删除', '用户删除', '/UserController/remove', 0, 4, 'system:user:remove', 2, 'entypo-trash', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (8, '用户修改', '用户修改', '/UserController/edit', 0, 4, 'system:user:edit', 2, 'fa fa-wrench', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (9, '角色管理', '角色展示', '/RoleController/view', 0, 411522822607867904, 'system:role:view', 1, 'fa fa-group', 2, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (10, '角色集合', '角色集合', '/RoleController/list', 0, 9, 'system:role:list', 2, '', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (11, '角色添加', '角色添加', '/RoleController/add', 0, 9, 'system:role:add', 2, 'entypo-plus-squared', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (12, '角色删除', '角色删除', '/RoleController/remove', 0, 9, 'system:role:remove', 2, 'entypo-trash', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (13, '角色修改', '角色修改', '/RoleController/edit', 0, 9, 'system:role:edit', 2, 'fa fa-wrench', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (14, '权限展示', '权限展示', '/PermissionController/view', 0, 411522822607867904, 'system:permission:view', 1, 'fa fa-key', 3, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (15, '权限集合', '权限集合', '/PermissionController/list', 0, 14, 'system:permission:list', 2, '', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (16, '权限添加', '权限添加', '/permissionController/add', 0, 14, 'system:permission:add', 2, 'entypo-plus-squared', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (17, '权限删除', '权限删除', '/PermissionController/remove', 0, 14, 'system:permission:remove', 2, 'entypo-trash', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (18, '权限修改', '权限修改', '/PermissionController/edit', 0, 14, 'system:permission:edit', 2, 'fa fa-wrench', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (19, '文件管理', '文件管理', '/FileController/view', 0, 592059865673760768, 'system:file:view', 1, 'fa fa-file-image-o', 4, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (20, '文件添加', '文件添加', '/FileController/add', 0, 19, 'system:file:add', 2, 'entypo-plus-squared', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (21, '文件删除', '文件删除', '/FileController/remove', 0, 19, 'system:file:remove', 2, 'entypo-trash', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (22, '文件修改', '文件修改', '/FileController/edit', 0, 19, 'system:file:edit', 2, 'fa fa-wrench', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (23, '文件集合', '文件集合', '/FileController/list', 0, 19, 'system:file:list', 2, '', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (330365026642825216, '公告管理', '公告展示', '/SysNoticeController/view', 0, 592059865673760768, 'gen:sysNotice:view', 1, 'fa fa-telegram', 10, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (331778807298134016, '定时器表达式', NULL, 'https://www.bejson.com/othertools/cron/', 1, 617766548966211584, '#', 1, 'layui-icon fa fa-flash', 12, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (332157860920299520, '定时任务', '定时任务调度表展示', '/SysQuartzJobController/view', 0, 592059865673760768, 'gen:sysQuartzJob:view', 1, 'fa fa-hourglass-1', 13, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (332857281479839744, '定时任务日志', '定时任务日志', '/SysQuartzJobLogController/view', 0, 592059865673760768, 'gen:sysQuartzJobLog:view', 1, 'fa fa-database', 14, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (335330315113467904, 'Json工具', NULL, 'https://www.bejson.com/jsonviewernew/', 1, 617766548966211584, '#', 1, 'layui-icon fa fa-retweet', 10, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (340067579836108800, '省份管理', NULL, '', 0, 0, '', 0, 'layui-icon layui-icon layui-icon-website', 4, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (340068151804956672, '省份表管理', '省份表展示', '/SysProvinceController/view', 0, 340067579836108800, 'gen:sysProvince:view', 1, 'fa fa-quora', 2, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (340088022018166784, '城市表管理', '城市设置展示', '/SysCityController/view', 0, 340067579836108800, 'gen:sysCity:view', 1, 'fa fa-quora', 3, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (340096183135506432, '地区设置管理', '地区设置展示', '/SysAreaController/view', 0, 340067579836108800, 'gen:sysArea:view', 1, 'fa fa-quora', 4, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (340127412270534656, '街道设置管理', '街道设置展示', '/SysStreetController/view', 0, 340067579836108800, 'gen:sysStreet:view', 1, 'fa fa-quora', 5, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (340301160042860544, '省份联动', '省份联动', '/ProvinceLinkageController/view', 0, 340067579836108800, '#', 1, 'fa fa-etsy', 1, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (340381240911859712, 'JavaScript格式化', NULL, '/static/tool/htmlformat/javascriptFormat.html', 0, 617766548966211584, '#', 1, 'layui-icon layui-icon fa fa-magic', 11, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (373489907429150720, 'URL拦截管理', '拦截url表展示', '/SysInterUrlController/view', 0, 617766548966211584, 'gen:sysInterUrl:view', 1, 'fa fa-hand-stop-o', 16, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (410791701859405824, '岗位管理', '岗位展示', '/SysPositionController/view', 0, 411522822607867904, 'gen:sysPosition:view', 1, 'fa fa-vcard', 17, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (410989805699207168, '部门管理', '部门展示', '/SysDepartmentController/view', 0, 411522822607867904, 'gen:sysDepartment:view', 1, 'fa fa-odnoklassniki', 18, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (411522822607867904, '用户管理', NULL, '', 0, 0, '', 0, 'layui-icon layui-icon-user', 3, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (486690002869157888, '用户密码修改', '用户密码修改', '/UserController/editPwd', 0, 4, 'system:user:editPwd', 2, 'entypo-tools', 3, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (496126970468237312, '日志展示', '日志管理', '/LogController/view', 0, 592059865673760768, 'system:log:view', 1, 'fa fa-info', 9, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (496127240363311104, '日志删除', '日志删除', '/LogController/remove', 0, 496126970468237312, 'system:log:remove', 2, 'entypo-trash', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (496127794879660032, '日志集合', '日志集合', '/LogController/list', 0, 496126970468237312, 'system:log:list', 2, NULL, NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (581541547099553792, 'druid监控', 'druid监控', '/druid/', 0, 617766548966211584, 'user:list', 1, 'fa fa-line-chart', 6, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (583063272123531264, 'API文档', NULL, '/doc.html', 1, 617766548966211584, '--', 1, 'layui-icon fa fa-font', 8, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (586003694080753664, '表单构建', NULL, '/static/component/code/index.html', 0, 617766548966211584, 'system:tool:view', 1, 'layui-icon layui-icon fa fa-list-alt', 5, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (587453033487532032, '后台模板', NULL, 'https://www.layui.com/doc/', 1, 617766548966211584, '', 1, 'layui-icon layui-icon fa fa-telegram', 9, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (592059865673760768, '系统管理', NULL, '', 0, 0, '', 0, 'layui-icon layui-icon layui-icon-home', 99, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (592167738407911424, '系统监控', '系统监控', '/ServiceController/view', 0, 617766548966211584, 'system:service:view', 1, 'fa fa-video-camera', 7, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (594691026430459904, '电子邮件管理', '电子邮件展示', '/EmailController/view', 0, 592059865673760768, 'system:email:view', 1, 'fa fa-envelope', 8, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (610635485890478080, '代码生成', NULL, '', 0, 0, '', 0, 'layui-icon layui-icon layui-icon layui-icon-praise', 2, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (610635950447394816, '全局配置', '', '/autoCodeController/global', 0, 610635485890478080, 'system:autocode:global', 1, 'fa fa-university', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (617766548966211584, '系统工具', NULL, '', 0, 0, '', 0, 'layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon-util', 5, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (618918631769636864, '字典管理', '字典类型表展示', '/DictTypeController/view', 0, 592059865673760768, 'system:dictType:view', 1, 'fa fa-puzzle-piece', 11, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (619836559427895296, '字典数据视图', '字典数据视图', '/DictDataController/view', 0, 618918631769636864, 'system:dictData:view', 2, NULL, NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3303650266428252171, '公告集合', '公告集合', '/SysNoticeController/list', 0, 330365026642825216, 'gen:sysNotice:list', 2, '', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3303650266428252182, '公告添加', '公告添加', '/SysNoticeController/add', 0, 330365026642825216, 'gen:sysNotice:add', 2, 'entypo-plus-squared', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3303650266428252193, '公告删除', '公告删除', '/SysNoticeController/remove', 0, 330365026642825216, 'gen:sysNotice:remove', 2, 'entypo-trash', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3303650266428252204, '公告修改', '公告修改', '/SysNoticeController/edit', 0, 330365026642825216, 'gen:sysNotice:edit', 2, 'fa fa-wrench', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3321578609202995211, '定时任务调度表集合', '定时任务调度表集合', '/SysQuartzJobController/list', 0, 332157860920299520, 'gen:sysQuartzJob:list', 2, '', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3321578609202995222, '定时任务调度表添加', '定时任务调度表添加', '/SysQuartzJobController/add', 0, 332157860920299520, 'gen:sysQuartzJob:add', 2, 'entypo-plus-squared', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3321578609202995233, '定时任务调度表删除', '定时任务调度表删除', '/SysQuartzJobController/remove', 0, 332157860920299520, 'gen:sysQuartzJob:remove', 2, 'entypo-trash', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3321578609202995244, '定时任务调度表修改', '定时任务调度表修改', '/SysQuartzJobController/edit', 0, 332157860920299520, 'gen:sysQuartzJob:edit', 2, 'fa fa-wrench', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3328572814798397451, '定时任务调度日志表集合', '定时任务调度日志表集合', '/SysQuartzJobLogController/list', 0, 332857281479839744, 'gen:sysQuartzJobLog:list', 2, '', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3328572814798397473, '定时任务调度日志表删除', '定时任务调度日志表删除', '/SysQuartzJobLogController/remove', 0, 332857281479839744, 'gen:sysQuartzJobLog:remove', 2, 'entypo-trash', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3400681518049566731, '省份表集合', '省份表集合', '/SysProvinceController/list', 0, 340068151804956672, 'gen:sysProvince:list', 2, '', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3400681518049566742, '省份表添加', '省份表添加', '/SysProvinceController/add', 0, 340068151804956672, 'gen:sysProvince:add', 2, 'entypo-plus-squared', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3400681518049566753, '省份表删除', '省份表删除', '/SysProvinceController/remove', 0, 340068151804956672, 'gen:sysProvince:remove', 2, 'entypo-trash', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3400681518049566764, '省份表修改', '省份表修改', '/SysProvinceController/edit', 0, 340068151804956672, 'gen:sysProvince:edit', 2, 'fa fa-wrench', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3400880220181667851, '城市设置集合', '城市设置集合', '/SysCityController/list', 0, 340088022018166784, 'gen:sysCity:list', 2, '', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3400880220181667862, '城市设置添加', '城市设置添加', '/SysCityController/add', 0, 340088022018166784, 'gen:sysCity:add', 2, 'entypo-plus-squared', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3400880220181667873, '城市设置删除', '城市设置删除', '/SysCityController/remove', 0, 340088022018166784, 'gen:sysCity:remove', 2, 'entypo-trash', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3400880220181667884, '城市设置修改', '城市设置修改', '/SysCityController/edit', 0, 340088022018166784, 'gen:sysCity:edit', 2, 'fa fa-wrench', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3400961831355064331, '地区设置集合', '地区设置集合', '/SysAreaController/list', 0, 340096183135506432, 'gen:sysArea:list', 2, '', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3400961831355064342, '地区设置添加', '地区设置添加', '/SysAreaController/add', 0, 340096183135506432, 'gen:sysArea:add', 2, 'entypo-plus-squared', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3400961831355064353, '地区设置删除', '地区设置删除', '/SysAreaController/remove', 0, 340096183135506432, 'gen:sysArea:remove', 2, 'entypo-trash', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3400961831355064364, '地区设置修改', '地区设置修改', '/SysAreaController/edit', 0, 340096183135506432, 'gen:sysArea:edit', 2, 'fa fa-wrench', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3401274122705346571, '街道设置集合', '街道设置集合', '/SysStreetController/list', 0, 340127412270534656, 'gen:sysStreet:list', 2, '', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3401274122705346582, '街道设置添加', '街道设置添加', '/SysStreetController/add', 0, 340127412270534656, 'gen:sysStreet:add', 2, 'entypo-plus-squared', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3401274122705346593, '街道设置删除', '街道设置删除', '/SysStreetController/remove', 0, 340127412270534656, 'gen:sysStreet:remove', 2, 'entypo-trash', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3401274122705346604, '街道设置修改', '街道设置修改', '/SysStreetController/edit', 0, 340127412270534656, 'gen:sysStreet:edit', 2, 'fa fa-wrench', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3734899074291507211, '拦截url表集合', '拦截url表集合', '/SysInterUrlController/list', 0, 373489907429150720, 'gen:sysInterUrl:list', 2, '', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3734899074291507222, '拦截url表添加', '拦截url表添加', '/SysInterUrlController/add', 0, 373489907429150720, 'gen:sysInterUrl:add', 2, 'entypo-plus-squared', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3734899074291507233, '拦截url表删除', '拦截url表删除', '/SysInterUrlController/remove', 0, 373489907429150720, 'gen:sysInterUrl:remove', 2, 'entypo-trash', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (3734899074291507244, '拦截url表修改', '拦截url表修改', '/SysInterUrlController/edit', 0, 373489907429150720, 'gen:sysInterUrl:edit', 2, 'fa fa-wrench', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (4107917018594058251, '岗位表集合', '岗位集合', '/SysPositionController/list', 0, 410791701859405824, 'gen:sysPosition:list', 2, '', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (4107917018594058262, '岗位表添加', '岗位添加', '/SysPositionController/add', 0, 410791701859405824, 'gen:sysPosition:add', 2, 'entypo-plus-squared', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (4107917018594058273, '岗位表删除', '岗位删除', '/SysPositionController/remove', 0, 410791701859405824, 'gen:sysPosition:remove', 2, 'entypo-trash', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (4107917018594058284, '岗位表修改', '岗位修改', '/SysPositionController/edit', 0, 410791701859405824, 'gen:sysPosition:edit', 2, 'fa fa-wrench', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (4109898056992071691, '部门集合', '部门集合', '/SysDepartmentController/list', 0, 410989805699207168, 'gen:sysDepartment:list', 2, '', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (4109898056992071702, '部门添加', '部门添加', '/SysDepartmentController/add', 0, 410989805699207168, 'gen:sysDepartment:add', 2, 'entypo-plus-squared', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (4109898056992071713, '部门删除', '部门删除', '/SysDepartmentController/remove', 0, 410989805699207168, 'gen:sysDepartment:remove', 2, 'entypo-trash', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (4109898056992071724, '部门修改', '部门修改', '/SysDepartmentController/edit', 0, 410989805699207168, 'gen:sysDepartment:edit', 2, 'fa fa-wrench', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (5946910264304599041, '电子邮件集合', '电子邮件集合', '/EmailController/list', 0, 594691026430459904, 'system:email:list', 2, '', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (5946910264304599042, '电子邮件添加', '电子邮件添加', '/EmailController/add', 0, 594691026430459904, 'system:email:add', 2, 'entypo-plus-squared', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (5946910264304599043, '电子邮件删除', '电子邮件删除', '/EmailController/remove', 0, 594691026430459904, 'system:email:remove', 2, 'entypo-trash', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (5946910264304599044, '电子邮件修改', '电子邮件修改', '/EmailController/edit', 0, 594691026430459904, 'system:email:edit', 2, 'fa fa-wrench', NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (6189186317738311681, '字典类型表集合', '字典类型表集合', '/DictTypeController/list', 0, 618918631769636864, 'system:dictType:list', 2, NULL, NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (6189186317948026882, '字典类型表添加', '字典类型表添加', '/DictTypeController/add', 0, 618918631769636864, 'system:dictType:add', 2, NULL, NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (6189186317948026883, '字典类型表删除', '字典类型表删除', '/DictTypeController/remove', 0, 618918631769636864, 'system:dictType:remove', 2, NULL, NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (6189186317989969924, '字典类型表修改', '字典类型表修改', '/DictTypeController/edit', 0, 618918631769636864, 'system:dictType:edit', 2, NULL, NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (6192095214866268161, '字典数据表集合', '字典数据表集合', '/DictDataController/list', 0, 618918631769636864, 'system:dictData:list', 2, NULL, NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (6192095214866268162, '字典数据表添加', '字典数据表添加', '/DictDataController/add', 0, 618918631769636864, 'system:dictData:add', 2, NULL, NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (6192095215075983363, '字典数据表删除', '字典数据表删除', '/DictDataController/remove', 0, 618918631769636864, 'system:dictData:remove', 2, NULL, NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_permission` VALUES (6192095215075983364, '字典数据表修改', '字典数据表修改', '/DictDataController/edit', 0, 618918631769636864, 'system:dictData:edit', 2, NULL, NULL, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_sys_permission_role
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_permission_role`;
CREATE TABLE `t_sys_permission_role`  (
  `id` bigint NOT NULL COMMENT '主键',
  `role_id` bigint NULL DEFAULT NULL COMMENT '角色id',
  `permission_id` bigint NULL DEFAULT NULL COMMENT '权限id',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色权限中间表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_permission_role
-- ----------------------------
INSERT INTO `t_sys_permission_role` VALUES (1502322385310273538, 1501447552788676610, 610635485890478080, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385310273539, 1501447552788676610, 610635950447394816, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385310273540, 1501447552788676610, 411522822607867904, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385310273541, 1501447552788676610, 9, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188098, 1501447552788676610, 10, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188099, 1501447552788676610, 11, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188100, 1501447552788676610, 12, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188101, 1501447552788676610, 13, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188102, 1501447552788676610, 14, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188103, 1501447552788676610, 15, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188104, 1501447552788676610, 16, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188105, 1501447552788676610, 17, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188106, 1501447552788676610, 18, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188107, 1501447552788676610, 340067579836108800, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188108, 1501447552788676610, 340301160042860544, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188109, 1501447552788676610, 340068151804956672, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188110, 1501447552788676610, 3400681518049566731, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188111, 1501447552788676610, 3400681518049566742, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188112, 1501447552788676610, 3400681518049566753, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188113, 1501447552788676610, 3400681518049566764, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188114, 1501447552788676610, 340088022018166784, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188115, 1501447552788676610, 3400880220181667851, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188116, 1501447552788676610, 3400880220181667862, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188117, 1501447552788676610, 3400880220181667873, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188118, 1501447552788676610, 3400880220181667884, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188119, 1501447552788676610, 340096183135506432, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188120, 1501447552788676610, 3400961831355064331, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188121, 1501447552788676610, 3400961831355064342, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385373188122, 1501447552788676610, 3400961831355064353, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102657, 1501447552788676610, 3400961831355064364, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102658, 1501447552788676610, 340127412270534656, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102659, 1501447552788676610, 3401274122705346571, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102660, 1501447552788676610, 3401274122705346582, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102661, 1501447552788676610, 3401274122705346593, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102662, 1501447552788676610, 3401274122705346604, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102663, 1501447552788676610, 617766548966211584, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102664, 1501447552788676610, 586003694080753664, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102665, 1501447552788676610, 581541547099553792, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102666, 1501447552788676610, 592167738407911424, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102667, 1501447552788676610, 583063272123531264, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102668, 1501447552788676610, 587453033487532032, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102669, 1501447552788676610, 335330315113467904, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102670, 1501447552788676610, 340381240911859712, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102671, 1501447552788676610, 331778807298134016, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102672, 1501447552788676610, 373489907429150720, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102673, 1501447552788676610, 3734899074291507211, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102674, 1501447552788676610, 3734899074291507222, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102675, 1501447552788676610, 3734899074291507233, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102676, 1501447552788676610, 3734899074291507244, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102677, 1501447552788676610, 592059865673760768, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102678, 1501447552788676610, 19, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102679, 1501447552788676610, 20, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102680, 1501447552788676610, 21, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102681, 1501447552788676610, 22, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102682, 1501447552788676610, 23, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102683, 1501447552788676610, 594691026430459904, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102684, 1501447552788676610, 5946910264304599041, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102685, 1501447552788676610, 5946910264304599042, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102686, 1501447552788676610, 5946910264304599043, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102687, 1501447552788676610, 5946910264304599044, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102688, 1501447552788676610, 496126970468237312, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102689, 1501447552788676610, 496127794879660032, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102690, 1501447552788676610, 496127240363311104, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102691, 1501447552788676610, 618918631769636864, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102692, 1501447552788676610, 619836559427895296, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385436102693, 1501447552788676610, 6189186317738311681, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385503211521, 1501447552788676610, 6192095214866268161, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385503211522, 1501447552788676610, 6189186317948026882, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385503211523, 1501447552788676610, 6192095214866268162, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385503211524, 1501447552788676610, 6189186317948026883, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385503211525, 1501447552788676610, 6192095215075983363, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385503211526, 1501447552788676610, 6192095215075983364, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385503211527, 1501447552788676610, 6189186317989969924, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385503211528, 1501447552788676610, 332157860920299520, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385503211529, 1501447552788676610, 3321578609202995211, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385503211530, 1501447552788676610, 3321578609202995222, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385503211531, 1501447552788676610, 3321578609202995233, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385503211532, 1501447552788676610, 3321578609202995244, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385503211533, 1501447552788676610, 332857281479839744, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385503211534, 1501447552788676610, 3328572814798397451, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1502322385503211535, 1501447552788676610, 3328572814798397473, 1, 'admin', '2022-03-11 16:35:43', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514497, 1515674373621112833, 411522822607867904, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514498, 1515674373621112833, 4, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514499, 1515674373621112833, 486690002869157888, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514500, 1515674373621112833, 5, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514501, 1515674373621112833, 6, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514502, 1515674373621112833, 7, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514503, 1515674373621112833, 8, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514504, 1515674373621112833, 9, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514505, 1515674373621112833, 10, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514506, 1515674373621112833, 11, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514507, 1515674373621112833, 12, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514508, 1515674373621112833, 13, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514509, 1515674373621112833, 14, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514510, 1515674373621112833, 15, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514511, 1515674373621112833, 410791701859405824, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514512, 1515674373621112833, 4107917018594058251, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514513, 1515674373621112833, 4107917018594058262, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514514, 1515674373621112833, 4107917018594058273, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801297514515, 1515674373621112833, 4107917018594058284, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801364623361, 1515674373621112833, 410989805699207168, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801364623362, 1515674373621112833, 4109898056992071691, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801364623363, 1515674373621112833, 4109898056992071702, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801364623364, 1515674373621112833, 4109898056992071713, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1515674801364623365, 1515674373621112833, 4109898056992071724, 1, '管理员', '2022-04-17 20:53:27', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746309423106, 488243256161730560, 411522822607867904, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746309423107, 488243256161730560, 4, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746359754754, 488243256161730560, 486690002869157888, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746359754755, 488243256161730560, 5, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746359754756, 488243256161730560, 6, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746422669313, 488243256161730560, 7, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746422669314, 488243256161730560, 8, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746422669315, 488243256161730560, 9, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746422669316, 488243256161730560, 10, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746464612353, 488243256161730560, 11, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746464612354, 488243256161730560, 12, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746464612355, 488243256161730560, 13, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746464612356, 488243256161730560, 410791701859405824, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746464612357, 488243256161730560, 4107917018594058251, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746506555393, 488243256161730560, 4107917018594058262, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746506555394, 488243256161730560, 4107917018594058273, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746506555395, 488243256161730560, 4107917018594058284, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746506555396, 488243256161730560, 410989805699207168, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746540109825, 488243256161730560, 4109898056992071691, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746540109826, 488243256161730560, 4109898056992071702, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746540109827, 488243256161730560, 4109898056992071713, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746540109828, 488243256161730560, 4109898056992071724, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746577858561, 488243256161730560, 617766548966211584, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746577858562, 488243256161730560, 592167738407911424, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746577858563, 488243256161730560, 592059865673760768, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746577858564, 488243256161730560, 19, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746640773121, 488243256161730560, 20, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746640773122, 488243256161730560, 21, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746640773123, 488243256161730560, 22, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746674327553, 488243256161730560, 23, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746674327554, 488243256161730560, 496126970468237312, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746674327555, 488243256161730560, 496127794879660032, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746674327556, 488243256161730560, 496127240363311104, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746674327557, 488243256161730560, 330365026642825216, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746674327558, 488243256161730560, 3303650266428252171, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746716270593, 488243256161730560, 3303650266428252182, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746716270594, 488243256161730560, 3303650266428252193, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);
INSERT INTO `t_sys_permission_role` VALUES (1519235746716270595, 488243256161730560, 3303650266428252204, 2, '开发人员', '2022-04-27 16:43:23', NULL, '', NULL);

-- ----------------------------
-- Table structure for t_sys_position
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_position`;
CREATE TABLE `t_sys_position`  (
  `id` bigint NOT NULL COMMENT '主键',
  `post_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位名称',
  `order_num` int NULL DEFAULT NULL COMMENT '排序',
  `status` int NULL DEFAULT NULL COMMENT '状态',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_position
-- ----------------------------
INSERT INTO `t_sys_position` VALUES (410792368778907648, '管理', 1, 1, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_position` VALUES (411477874382606336, '推广', 99, 1, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_position` VALUES (1501448436742438914, '任务发布', 2, 1, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_sys_province
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_province`;
CREATE TABLE `t_sys_province`  (
  `id` bigint NOT NULL COMMENT '主键',
  `province_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省份代码',
  `province_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省份名称',
  `short_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '简称',
  `lng` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '纬度',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `data_state` bigint NULL DEFAULT NULL COMMENT '状态',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `Index_1`(`province_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '省份表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_province
-- ----------------------------
INSERT INTO `t_sys_province` VALUES (22, '500000', '重庆', '重庆', '106.504959', '29.533155', 22, 0, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_sys_quartz_job
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_quartz_job`;
CREATE TABLE `t_sys_quartz_job`  (
  `id` bigint NOT NULL COMMENT '主键',
  `job_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务组名',
  `invoke_target` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'cron执行表达式',
  `misfire_policy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'cron计划策略',
  `concurrent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否并发执行（0允许 1禁止）',
  `status` int NULL DEFAULT NULL COMMENT '任务状态（0正常 1暂停）',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_quartz_job
-- ----------------------------
INSERT INTO `t_sys_quartz_job` VALUES (332182389491109888, 'v2Task2', 'SYSTEM', 'v2Task.runTask2(1,2l,\'asa\',true,2D)', '*/5 * * * * ?', '2', '0', 1, 1, 'admin', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_quartz_job` VALUES (1508083947423170562, '自动结束过期任务', 'SYSTEM', 'v2Task.runTask1()', '0 0/10 * * * ?', '1', '0', 1, 433236479427350528, '开发人员', '2022-03-27 22:10:07', 433236479427350528, '', '2022-04-23 23:49:21');

-- ----------------------------
-- Table structure for t_sys_quartz_job_log
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_quartz_job_log`;
CREATE TABLE `t_sys_quartz_job_log`  (
  `id` bigint NOT NULL COMMENT '主键',
  `job_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务组名',
  `invoke_target` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '调用目标字符串',
  `job_message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` int NULL DEFAULT NULL COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '异常信息',
  `start_time` datetime NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime NULL DEFAULT NULL COMMENT '结束时间',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_quartz_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for t_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role`;
CREATE TABLE `t_sys_role`  (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_role
-- ----------------------------
INSERT INTO `t_sys_role` VALUES (488243256161730560, '管理员', 1, '管理员', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_role` VALUES (1501447552788676610, '开发', 1, '管理员', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_role` VALUES (1515674373621112833, '人员管理', 1, '管理员', '2022-04-17 20:51:45', NULL, '', NULL);

-- ----------------------------
-- Table structure for t_sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role_user`;
CREATE TABLE `t_sys_role_user`  (
  `id` bigint NOT NULL COMMENT '主键',
  `sys_user_id` bigint NULL DEFAULT NULL COMMENT '用户id',
  `sys_role_id` bigint NULL DEFAULT NULL COMMENT '角色id',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色中间表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_role_user
-- ----------------------------
INSERT INTO `t_sys_role_user` VALUES (495571139645542400, 1, 488243256161730560, 1, '管理员', '2022-03-09 22:12:28', NULL, NULL, NULL);
INSERT INTO `t_sys_role_user` VALUES (1516411336167280641, 2, 1501447552788676610, 1, '管理员', '2022-04-19 21:40:11', NULL, '', NULL);
INSERT INTO `t_sys_role_user` VALUES (1519266355836674050, 1519266355836674049, 1515674373621112833, 1, '管理员', '2022-04-27 18:45:01', NULL, '', NULL);

-- ----------------------------
-- Table structure for t_sys_street
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_street`;
CREATE TABLE `t_sys_street`  (
  `id` bigint NOT NULL COMMENT '主键',
  `street_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '街道代码',
  `area_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父级区代码',
  `street_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '街道名称',
  `short_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '简称',
  `lng` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '纬度',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `data_state` int NULL DEFAULT NULL COMMENT '状态',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `Index_1`(`street_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '街道设置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_street
-- ----------------------------

-- ----------------------------
-- Table structure for t_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user`;
CREATE TABLE `t_sys_user`  (
  `id` bigint NOT NULL COMMENT '主键',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户账号',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户密码',
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '昵称',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '真实姓名',
  `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '00' COMMENT '用户类型（00系统用户 01注册用户）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '手机号码',
  `qq` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'qq号',
  `qq_openid` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'qq统一openid',
  `qq_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'qq昵称',
  `qq_figureurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'qq头像',
  `add_qq_group_html` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '一键加群html',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '头像路径',
  `dep_id` bigint NULL DEFAULT NULL COMMENT '部门id',
  `pos_id` bigint NULL DEFAULT NULL COMMENT '岗位id',
  `collection_account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收款账号',
  `extension_id` bigint NULL DEFAULT NULL COMMENT '推荐人Id',
  `extension_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '推荐人名称',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint NULL DEFAULT NULL COMMENT '修改人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `qq_openid`(`qq_openid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_user
-- ----------------------------
INSERT INTO `t_sys_user` VALUES (1, 'admin', 'KRhTkchzDx7hEuHAFnyE0tTiwmydTqzI', '管理员', NULL, '00', 'test@foxmail.com', '18688888888', NULL, NULL, NULL, NULL, NULL, '0', '', 1, 410792368778907648, NULL, NULL, NULL, 1, 'admin', '2022-03-09 22:12:28', 1, '管理员', '2022-04-17 11:10:15');
INSERT INTO `t_sys_user` VALUES (2, 'kaifa', 'Wg0KYtcR80upCXw5RVZFhGqWjdJOWTDW', '开发人员', '', '00', 'test@foxmail.com', '18688888888', '', NULL, '', NULL, '', '0', '', 1, 411477874382606336, '', NULL, NULL, 1, 'admin', '2022-03-09 22:12:28', 1, '管理员', '2022-04-19 21:40:11');
INSERT INTO `t_sys_user` VALUES (1519266355836674049, 'test', 'gO5l7gtqxLi48Jyi79KfhTzzv1n5LYOCJZ189YJk9Vk=', 'test', '邓强', '00', 'dqfe123@163.com', '15871383472', '256315263', NULL, 'test', NULL, '', '0', '', 1, 410792368778907648, '', NULL, NULL, 1, '管理员', '2022-04-27 18:45:01', NULL, '', NULL);

SET FOREIGN_KEY_CHECKS = 1;
