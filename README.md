# 52rsh
本项目在SpringBoot_v2项目的基础上集成了redis,实现了shiro+reids实现了重启，不重新登录。（原本是按单点登录做的，但本项目面对的是小型企业用户或个人用户，单机运行的就没有定义为单点登录）。

集成了QQ登录，在shiro的基础是添加了QQ登录，原理上是可以实现其他的任何一种第三方登录，手机短信、CA认证。目前因只用到了QQ登录所有就没有继续开发.有需要的同学可以自行完善。

调整了文件传输，使用本地文件系统。可以少装一个minio.也减少了大家的学习成本和一般人员的文件维护成本。

调整了密码策略，原有的MD5策略是为了防止黑客获取到数据库后可以直接获取明文才处理的，但MD5依然存在碰撞算法，可以破译出来。但是也带来了系统也不知道用户的原始密码，在做统一密码策略调整时，基本为不可能的问题。现在使用jasypt，可以自定义设计密码规则，如果后期出现问题，可以对所有密码进行统一调整，保证更安全。

实体添加了2种，现有三种，只有主键id,  添加创建者3字段  再次添加更新者三字段

自定义日志文件地址，windows下会自动添加盘符（本地开发或windows服务器）。

以上改动是实际业务需求做出的调整。

### 前言
项目名称为"shr25"，想表达的意思 “树人爱我”，“树人助人”，反过来为“52rsh”，“我爱人生”。

本项目的来源：
母项目：SpringBoot_v2  [https://gitee.com/bdj/SpringBoot_v2](https://gitee.com/bdj/SpringBoot_v2)  
当前项目：shr25 [https://gitee.com/shr25/shr25](https://gitee.com/shr25/shr25)  
继承了SpringBoot_v2作者的思想， 完全开源， 不收取任何费用  
下面是SpringBoot_v2作者的原话，在其项目介绍中可以找到  
```
1. 没有基础版、没有vip版本、没有付费群、没有收费二维码
2. 遵循开源真谛，一切免费才是真开源
3. 不求回报，你使用快乐就是这个项目最大的快乐！
```

### 开发环境，如有需要可以联系我们
- JDK8.0
- redis-6.0.7以上
- mysql5.7以上

### 资源下载
- JDK8 http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
- Maven http://maven.apache.org/download.cgi
- minio http://www.minio.org.cn/ （文件存储）


### 部署流程
1. 导入doc文件夹里面的springbootv2.sql到数据库
2. 确认自己的mysql版本 进行修改jar  在pom.xml 73-84行
3. 修改application-dev.yml 里面自己数据库版本对应的jdbc链接
4. 正常启动run 
- wiki暂时没有，使用SpringBoot_v2地址:https://gitee.com/bdj/SpringBoot_v2/wikis

### 打包发布编译流程
- maven编译安装pom.xml文件即可打包成jar

编译完整可执行jar，需要调整pom.xml
```
<plugin>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-maven-plugin</artifactId>
    <configuration>
        <mainClass>com.shr25.V2Application</mainClass>
        <layout>ZIP</layout>
        <includes>
            <include>
                <groupId>nothing</groupId>
                <artifactId>nothing</artifactId>
            </include>
        </includes>
    </configuration>
</plugin>
```
调整为：
```
<plugin>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-maven-plugin</artifactId>
</plugin>
```

### 发布启动方式
完整打包可以调整端口号，环境
java -jar shr25-0.0.1.jar --server_port=8082 --spring_profiles_active=test  &

不打包jar,减少文件大小，发布时间，手动上传jar后，无需每次上传。
java -Dloader.path=\lib -jar shr25-0.0.1.jar --server_port=8082 --spring_profiles_active=test  &


### 登陆地址
- 服务器:http://www.shr25.com/ 不提供演示账号。如有需要可以本地搭建
- 本地 http://localhost:8080   默认帐号密码: admin/admin  kaifa/kaifa
- swagger  http://localhost:8080/swagger-ui.html

### 启动类
- SpringbootStart 启动类

### 新界面风格
|![输入图片说明](https://images.gitee.com/uploads/images/2021/0101/134930_749be44d_123301.png "屏幕截图.png")  | ![输入图片说明](https://images.gitee.com/uploads/images/2021/0101/135013_703123c3_123301.png "屏幕截图.png")    |
| --- | --- |
|![输入图片说明](https://images.gitee.com/uploads/images/2021/0101/135035_24fdcafb_123301.png "屏幕截图.png")    | ![输入图片说明](https://images.gitee.com/uploads/images/2021/0101/135102_7ba9097f_123301.png "屏幕截图.png")     |

### 数据库模型
![数据库模型](https://images.gitee.com/uploads/images/2019/0701/001953_dd7a387e_123301.png "数据库模型.png")


### 后台代码注释风格

|![后台代码](https://images.gitee.com/uploads/images/2018/0909/203106_52eca8e3_123301.jpeg "1.jpg")   | ![后台代码](https://images.gitee.com/uploads/images/2018/0909/203112_278db2f4_123301.jpeg "2.jpg")  |
|---|---|
|![后台代码](https://images.gitee.com/uploads/images/2018/0909/203118_39d8b7cd_123301.jpeg "3.jpg")   |  ![后台代码](https://images.gitee.com/uploads/images/2018/0909/203125_a362822a_123301.jpeg "4.jpg") |


### 前端代码注释风格
![HTML代码页面](https://images.gitee.com/uploads/images/2018/0822/004608_c55d62a4_123301.jpeg "HTML代码页面.jpg")
![js引入](https://images.gitee.com/uploads/images/2018/0909/203322_6dc467c2_123301.jpeg "js引入.jpg")


### 情况说明
- 如果您喜欢shr25，可以clone下来使用，您的star将是本人前进的动力，如果您有技术疑问，可以加群交流。
- 如果shr25对您有一点帮助，您可以点个star，就是对作者最大的支持了。
- shr25脚手架会一直更新下去。
- 需要进项目一起开发的请进群私聊我，让我们一起维护这个开发项目

### 开发者联系
进群方式提供了两种，原因为：QQ的BUG,点击加群无法兼容PC和移动端。如果已经加群了，则任意一个都可以进群。群号：663816139.
<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=q0rU_yLuu-e5QDBN2f-MjyIQEVJuBxPF&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="shr25交流群" title="shr25交流群">移动端点击进群</a> | <a target="_blank" href="https://shang.qq.com/wpa/qunwpa?idkey=323fe4cf2a01c190c02b56fb46585d85665054b6f3c36068409a65c32bd02dc3"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="shr25交流群" title="shr25交流群">PC端点击进群</a>

有任何问题可以提出


### 注意事项
- 所有的model字段解释都在mysql的字段注释里面，请具体查看mysql的字段注解

### wiki
暂时没有，还不知道怎么创建，大家可以一起建设

### 项目视频列表
暂时没有，还不知道怎么创建，大家可以一起建设

### 更新日志
暂时没有，还不知道怎么创建，大家可以一起建设

### 参与开源作者

![荣誉殿堂](https://images.gitee.com/uploads/images/2019/0903/224111_37b4d05e_123301.png "荣誉殿堂.png")


| 名字        | 联系方式                | 贡献功能           | 其他                                   |
| ----------- | ----------------------- | ------------------ | -------------------------------------- |
| 火冰        |QQ344420550             |    前后端全栈     |   面前有一个兴趣three.js，3d web 开发   |


### 原SpringBoot_v2借鉴项目列表
- https://gitee.com/renrenio/renren-generator 人人得代码自动生成，改成自动录入数据库
- https://gitee.com/y_project/RuoYi-fast 借鉴ry.js




### 精品项目推荐

|项目名称 | 项目地址 | 项目介绍 |
|---|---|---|
| Jpom GVP项目      | [https://gitee.com/dromara/Jpom](https://gitee.com/dromara/Jpom) | 一款简而轻的低侵入式在线构建、自动部署、日常运维、项目监控软件|
| AgileBPM GVP项目  | [https://gitee.com/agile-bpm](https://gitee.com/agile-bpm)  | 专注于解决企业工作流实施难的问题  |
| AlibabaCloud     | [https://gitee.com/matevip/matecloud](https://gitee.com/matevip/matecloud)  | MateCloud是一款基于Spring Cloud Alibaba的微服务架构 |
| ApiBoot | [https://gitee.com/minbox-projects/api-boot](https://gitee.com/minbox-projects/api-boot)    | 为接口服务而生  |
| v2皮肤           | [https://gitee.com/Jmysy/Pear-Admin-Layui](https://gitee.com/pear-admin/Pear-Admin-Layui)                   |

### Java公众号推荐:
如果大家有公众号，可以申请添加
