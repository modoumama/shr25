// 预览
function preView(obj){
    var pimg = obj;
    // var pimg = document.querySelector("img");
    var oImg = document.querySelector(".box img");
    var oBox = document.querySelector(".box");
    // pimg.onclick=function(){
    oBox.style.display="flex"
    oImg.src=pimg.src
    // }
    oBox.onclick=function(){
        oBox.style.display="none"
        oImg.src=''
    }
    var hammer = new Hammer(oImg);//hammer实例化
    hammer.get('pan').set({direction: Hammer.DIRECTION_ALL});//激活pan(移动)功能
    hammer.get('pinch').set({enable: true});//激活pinch(双指缩放)功能
    hammer.on("panstart", function(event) {
        var left = oImg.offsetLeft;
        var tp = oImg.offsetTop;
        hammer.on("panmove", function(event) {
            oImg.style.left = left + event.deltaX + 'px';
            oImg.style.top = tp + event.deltaY + 'px';
        });
    })

    hammer.on("pinchstart", function(e) {
        hammer.on("pinchout", function(e) {
            oImg.style.transition = "-webkit-transform 300ms ease-out";
            oImg.style.webkitTransform = "scale(2.5)";
        });
        hammer.on("pinchin", function(e) {
            oImg.style.transition = "-webkit-transform 300ms ease-out";
            oImg.style.webkitTransform = "scale(1)";
        });
    })
}

// 创建数组保存图片
var files = new Array();
var id = 0;
// 选择图片按钮隐藏input[file]
$("#btn-upload").click(function() {
    $("#file").trigger("click");
});
// 选择图片
$("#file").change(function() {
    // 获取所有图片
    var img = document.getElementById("file").files;
    // 遍历
    for (var i = 0; i < img.length; i++) {
        // 得到图片
        var file = img[i];
        // 判断是否是图片

        var flag = judgeImgSuffix(file.name);
        if(flag){

        }else{
            alert("要求图片格式为png,jpg,jpeg,bmp");
            return;
        }

        // 把图片存到数组中
        files[id] = file;
        id++;
        // 获取图片路径
        var url = URL.createObjectURL(file);

        // 创建img
        var box = document.createElement("img");
        box.setAttribute("src", url);
        box.className = 'img';
        box.onclick = function(){
            preView(this);
        };

        // 创建div
        var imgBox = document.createElement("div");
        imgBox.style.float = 'left';
        imgBox.className = 'img-item';

        // 创建span
        var deleteIcon = document.createElement("span");
        deleteIcon.className = 'delete';
        deleteIcon.innerText = 'x';
        // 把图片名绑定到data里面
        deleteIcon.id = img[i].name;
        // 把img和span加入到div中
        imgBox.appendChild(deleteIcon);
        imgBox.appendChild(box);
        // 获取id=gallery的div
        var body = document.getElementsByClassName("gallery")[0];
        // body.appendChild(imgBox);
        var showPlace =document.getElementsByClassName("img-item")[0];
        body.insertBefore(imgBox,showPlace);
        // 点击span事件
        $(deleteIcon).click(function() {
            // 获取data中的图片名
            var filename = $(this).attr('id');
            // 删除父节点
            $(this).parent().remove();
            var fileList = Array.from(files);
            // 遍历数组
            for (var j = 0; j < fileList.length; j++) {
                // 通过图片名判断图片在数组中的位置然后删除
                if (fileList[j].name == filename) {
                    fileList.splice(j, 1);
                    id--;
                    break;
                }
            }
            files = fileList;
        });
    }
});

// 判断是否是图片类型
function judgeImgSuffix(path) {
    var index = path.lastIndexOf('.');
    var suffix = "";
    if (index > 0) {
        suffix = path.substring(index + 1);
    }
    if ("png" == suffix || "jpg" == suffix || "jpeg" == suffix || "bmp" == suffix || "PNG" == suffix || "JPG" == suffix || "JPEG" == suffix || "BMP" == suffix) {
        return true;
    } else {
        return false;
    }
}

//显示大图片
function show_img(img) {
    //页面层
    layer.open({
        type: 1,
        skin: 'layui-layer-rim', //加上边框
        area: ['80%', '80%'], //宽高
        shadeClose: true, //开启遮罩关闭
        end: function (index, layero) {
            return false;
        },
        content: '<div style="text-align:center"><img style="width: 100%" src="' + $(img).attr('src') + '" /></div>'
    });
}