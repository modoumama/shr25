//显示大图片
function show_img(img) {
    //页面层
    layer.open({
        type: 1,
        skin: 'layui-layer-rim', //加上边框
        area: ['80%', '80%'], //宽高
        shadeClose: true, //开启遮罩关闭
        end: function (index, layero) {
            return false;
        },
        content: '<div style="text-align:center"><img style="width: 100%" src="' + $(img).attr('src') + '" /></div>'
    });
}

//调用示例
$("body").on("click", ".img", function(e){
    e.stopPropagation();
    show_img(this)
});