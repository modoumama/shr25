//添加ajax全局变量
 var mask;
 (function(windows){
 	var r20 = /%20/g,
	rbracket = /\[\]$/;
 	jQuery.param = function( a, traditional ) {
		var prefix,
			s = [],
			add = function( key, value ) {
				// If value is a function, invoke it and return its value
				value = jQuery.isFunction( value ) ? value() : ( value == null ? "" : value );
				s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
			};

		// Set traditional to true for jQuery <= 1.3.2 behavior.
		if ( traditional === undefined ) {
			traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
		}

		// If an array was passed in, assume that it is an array of form elements.
		if ( jQuery.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {
			// Serialize the form elements
			jQuery.each( a, function() {
				add( this.name, this.value );
			});

		} else {
			// If traditional, encode the "old" way (the way 1.3.2 or older
			// did it), otherwise encode params recursively.
			for ( prefix in a ) {
				buildParams( prefix, a[ prefix ], traditional, add );
			}
		}

		// Return the resulting serialization
		return s.join( "&" ).replace( r20, "+" );
	};

	function buildParams( prefix, obj, traditional, add ) {
		var name;

		if ( jQuery.isArray( obj ) ) {
			// Serialize array item.
			jQuery.each( obj, function( i, v ) {
				if ( traditional || rbracket.test( prefix ) ) {
					// Treat each array item as a scalar.
					add( prefix, v );

				} else {
					// Item is non-scalar (array or object), encode its numeric index.
					//buildParams( prefix + "[" + ( typeof v === "object" ? i : "" ) + "]", v, traditional, add );
					// --------------修改上传参数-------------------
					buildParams( prefix + "[" + i  + "]", v, traditional, add );
				}
			});

		} else if ( !traditional && jQuery.type( obj ) === "object" ) {
			// Serialize object item.
			for ( name in obj ) {
				buildParams( prefix + "." + name, obj[ name ], traditional, add );
			}

		} else {
			// Serialize scalar item.
			add( prefix, obj );
		}
	}
 })(window);
$.ajaxSetup({
	cache:false, 
	//timeout:3000,
	//IE9浏览器ajax的跨域问题
    crossDomain: true == !(document.all), 
//  crossDomain: true,
//  processData: false,
   // xhrFields:{
     // withCredentials:true
   // },
    beforeSend: function(xhr){
//  	debugger;
       var token = $.getToken();
       xhr.setRequestHeader("Token",token);
       mask =top.layer.load(0, {shade: [0.3, '#000000']});

    },
    complete: function (xhr,textStatus,errorThrown) {
        top.layer.close(mask);
	   	try{
	   		if("timeout" == textStatus){
	   			top.layer.alert("请求超时稍后再试！");
	   		}else{
	   			switch(xhr.status){
                    case 403:
                         top.layer.alert("系统拒绝：您没有访问权限,请联系管理员！");
                         break;
                    case 404:
                         top.layer.alert("您访问的资源不存在，请检查网络是否链接正常或是访问的资源已被删除！");
                         break;
                    case 500:
                         top.layer.alert("服务器异常，错误信息：" + errorThrown);
                         break;
				case 200:
					//tonken是否失效，跳转登录地址
					var REDIRECT_STATUS = xhr.getResponseHeader('REDIRECT_STATUS');
					if(REDIRECT_STATUS){
                        //记录token失效事件
						sessionStorage.setItem("unTonkenText","tonken失效:200:"+this.url);
						sessionStorage.setItem("lastTonkenTime",(new Date().getTime()));
                        var win = window;
                        while(win != win.top){
                            win = win.top;
                        }
						if(win.employeeType && win.employeeType == 9){
                            win.location.href = win.siteInfo.expertUrl;
						}else{
                            win.location.href = win.siteInfo.loginUrl;
						}
					}
					break;
                }
	   		}
		}catch(e){}
    }
});
//接收参数
 $.getUrlParam = function (name) {
     var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象  
     var r = window.location.search.substr(1).match(reg);  //匹配目标参数  
     if (r != null) {
        return unescape(r[2]); 
     }else{
     	return null; //返回参数值
     }
}  
if($(".prieUnit").length > 0 && top.prieUnit){	
	$(".prieUnit").html(top.prieUnit);
}
//url添加token
$.parserUrlForToken = function(url){
	if(url.indexOf("?") > 0){
		url += "&Token=" + $.getToken();
	}else{
		url += "?Token=" + $.getToken();
	}
	return encodeURI(url);
}
//获取token
 $.getToken = function () {
     //var token = $.getUrlParam("Token");
     var token;
     if($("#Token").val()){
     	token = $("#Token").val();
     } else {
     	token = $.getUrlParam("Token");
     }

     if( typeof(token)=="undefined" || token == null || token == ""){
         token = top.$("#Token").val();
     }

     if( typeof(token)=="undefined" || token == null || token == ""){
         token = sessionStorage.getItem('token');
     }
   return token;
} 
