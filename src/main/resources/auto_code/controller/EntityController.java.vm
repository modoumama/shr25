package ${parentPack}.controller.${model};

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import ${parentPack}.common.base.BaseController;
import ${parentPack}.common.domain.AjaxResult;
import ${parentPack}.common.domain.ResultTable;
import ${parentPack}.model.custom.Tablepar;
import ${parentPack}.model.${model}.${tableInfo.javaTableName};
import ${parentPack}.service.${model}.I${tableInfo.javaTableName}Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
#set($isdate = false)
#set($isupload = false)
#foreach ($column in $beanColumns)
#if(${column.column_key} != 'PRI')
#if(${column.htmlType} == 4)
#set($isdate =true)
#elseif(${column.htmlType} == 5)
#set($isupload =true)
#end
#end
#end
#if($isupload==true)
import com.shr25.v2.common.conf.oss.OssConfig;
#end

/**
 * ${tableInfo.tableComment} Controller
 *
 * @author ${author}
 * @date ${datetime}
 */
@Api(value = "${tableInfo.tableComment}")
@Controller
@RequestMapping("/${tableInfo.javaTableName}Controller")
public class ${tableInfo.javaTableName}Controller extends BaseController {

    private String prefix = "${model}/${tableInfo.javaTableName_a}";

    @Autowired
    private I${tableInfo.javaTableName}Service ${tableInfo.javaTableName_a}Service;


    /**
     * ${tableInfo.tableComment}页面展示
     *
     * @param model
     * @return String
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @RequiresPermissions("${permissionPrefix}:${tableInfo.javaTableName_a}:view")
#if($isupload==true)
    @OssConfig
#end
    public ModelAndView view(ModelMap model) {
       return modelAndView(prefix + "/list");
    }

    /**
     * list集合
     *
     * @param tablepar
     * @return ResultTable
     */
    //@Log(title = "${tableInfo.tableComment}", action = "111")
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/list")
    @RequiresPermissions("${permissionPrefix}:${tableInfo.javaTableName_a}:list")
    @ResponseBody
    public ResultTable list(Tablepar tablepar) {
        QueryWrapper<${tableInfo.javaTableName}> queryWrapper = new QueryWrapper<${tableInfo.javaTableName}>();

        if (StrUtil.isNotEmpty(tablepar.getSearchText())) {
            queryWrapper.like("自定义", tablepar.getSearchText());
        }

        if (StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
            queryWrapper.orderByAsc(tablepar.getOrderByColumn());
        }

        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());

        PageInfo<${tableInfo.javaTableName}> page = new PageInfo<${tableInfo.javaTableName}>(${tableInfo.javaTableName_a}Service.list(queryWrapper));
        return pageTable(page.getList(), page.getTotal());

    }

    /**
     * 新增跳转
     *
     * @param modelMap
     * @return
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
#if($isupload==true)
    @OssConfig
#end
    public ModelAndView add(ModelMap modelMap) {
       return modelAndView(prefix + "/add");
    }

    /**
     * 新增保存
     *
     * @param ${tableInfo.javaTableName_a}
     * @return
     */
    //@Log(title = "${tableInfo.tableComment}新增", action = "111")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @RequiresPermissions("${permissionPrefix}:${tableInfo.javaTableName_a}:add")
    @ResponseBody
    public AjaxResult add(${tableInfo.javaTableName} ${tableInfo.javaTableName_a}) {
		return toAjax(${tableInfo.javaTableName_a}Service.save(${tableInfo.javaTableName_a}));
    }

    /**
     * ${tableInfo.tableComment}删除
     *
     * @param ids
     * @return
     */
    //@Log(title = "${tableInfo.tableComment}删除", action = "111")
    @ApiOperation(value = "删除", notes = "删除")
    @DeleteMapping("/remove")
    @RequiresPermissions("${permissionPrefix}:${tableInfo.javaTableName_a}:remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
		return toAjax(${tableInfo.javaTableName_a}Service.removeByIds(ids));
    }


    /**
     * 修改跳转
     *
     * @param id
     * @param map
     * @return
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
#if($isupload==true)
    @OssConfig
#end
    public ModelAndView edit(@PathVariable("id") Long id, ModelMap map) {
        map.put("${tableInfo.javaTableName}", ${tableInfo.javaTableName_a}Service.getById(id));
       return modelAndView(prefix + "/edit");
    }

    /**
     * 修改保存
     *
     * @param ${tableInfo.javaTableName_a}
     * @return
     */
    //@Log(title = "${tableInfo.tableComment}修改", action = "111")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("${permissionPrefix}:${tableInfo.javaTableName_a}:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(${tableInfo.javaTableName} ${tableInfo.javaTableName_a}) {
        return toAjax(${tableInfo.javaTableName_a}Service.updateById(${tableInfo.javaTableName_a}));
    }


    /**
     * 修改状态
     *
     * @param ${tableInfo.javaTableName_a}
     * @return
     */
    @PutMapping("/updateVisible")
    @ResponseBody
    public AjaxResult updateVisible(@RequestBody ${tableInfo.javaTableName} ${tableInfo.javaTableName_a}) {
        return toAjax(${tableInfo.javaTableName_a}Service.updateById(${tableInfo.javaTableName_a}));
    }
}
