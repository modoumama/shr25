package com.shr25.v2.controller;

import com.amazonaws.services.s3.model.PutObjectResult;
import com.shr25.v2.common.base.BaseController;
import com.shr25.v2.common.commons.FileType;
import com.shr25.v2.common.domain.AjaxResult;
import com.shr25.v2.common.file.LocalFile;
import com.shr25.v2.common.file.MultipartToLoaclFileUtis;
import com.shr25.v2.model.admin.TSysFile;
import com.shr25.v2.model.admin.TSysUser;
import com.shr25.v2.shiro.util.ShiroUtils;
import com.shr25.v2.util.SnowflakeIdWorker;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 文件操作
 * @ClassName: FileController
 * @author huobing
 * @date 2022-06-1 14:30
 */
@Slf4j
@Api(tags = "文件模块")
@RestController
public class FileController extends BaseController{
	/**
	 * wangEditor上传文件
	 *
	 * @param file
	 * @return
	 */
	@ApiOperation(value = "上传文件", notes = "上传文件")
	@PostMapping("/upload-img")
	public AjaxResult updateAvatar(MultipartFile file) throws IOException {
		if (!file.isEmpty()){
			LocalFile img =  MultipartToLoaclFileUtis.toAttachmentFile(file, "/wangEditor");
			AjaxResult ajaxResult = success();
			Map<String, Object> data = new HashMap<>();
			data.put("url", img.getFileUrl());
			ajaxResult.put("errno", 0);
			ajaxResult.put("data", data);
			return ajaxResult;
		}else {
			return error();
		}
	}
}
