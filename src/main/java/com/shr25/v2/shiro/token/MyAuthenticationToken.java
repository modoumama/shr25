package com.shr25.v2.shiro.token;

import com.shr25.v2.model.admin.TSysUser;
import org.apache.shiro.authc.AuthenticationToken;

public class MyAuthenticationToken implements AuthenticationToken {
  private TSysUser user;

  private String loginType;

  public MyAuthenticationToken(TSysUser user, String loginType) {
    this.user = user;
    this.loginType = loginType;
  }

  @Override
  public Object getPrincipal() {
    return user;
  }

  @Override
  public Object getCredentials() {
    return loginType;
  }
}
