package com.shr25.v2.shiro.service;

import com.shr25.v2.model.admin.TSysPermission;
import com.shr25.v2.model.admin.TSysRole;
import com.shr25.v2.model.admin.TSysUser;
import com.shr25.v2.service.admin.ITSysPermissionService;
import com.shr25.v2.service.admin.ITSysRoleService;
import com.shr25.v2.service.admin.ITSysUserService;
import com.shr25.v2.shiro.token.MyAuthenticationToken;
import com.shr25.v2.util.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * 身份校验核心类--免登录（第三方登录）
 *
 * @author huobing
 * @className: LoginFreeShiroRealm
 * @date 2022年04月12日
 */
@Service
public class LoginFreeShiroRealm extends AuthorizingRealm {
    /**
     * 用户
     */
    @Autowired
    private ITSysUserService userService;

    /**
     * 权限
     */
    @Autowired
    private ITSysPermissionService permissionService;

    /**
     * 角色
     */
    @Autowired
    private ITSysRoleService roleService;

    @PostConstruct
    public void initMatcher(){
        setAuthenticationTokenClass(MyAuthenticationToken.class);
    }

    /**
     * 认证登陆
     * @param token
     * @return authenticationInfo
     * @throws
     */
    @SuppressWarnings("unused")
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        //加这一步的目的是在Post请求的时候会先进认证，然后在到请求        
        if (StringUtils.isNull(token.getPrincipal())) {
            return null;
        }

        TSysUser userInfo = (TSysUser) token.getPrincipal();

        return new SimpleAuthenticationInfo(userInfo, token.getCredentials(), getName());
    }

    /**
     * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用.
     * @param principals
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        if (StringUtils.isNull(principals)) {
            throw new AuthorizationException("principals should not be null");
        }

        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        TSysUser userinfo = (TSysUser) principals.getPrimaryPrincipal();
        Long uid = userinfo.getId();
        List<TSysRole> tsysRoles = roleService.queryUserRole(uid);
        tsysRoles.forEach(userrole -> {
            //添加角色名字
            authorizationInfo.addRole(userrole.getName());
            List<TSysPermission> permissions = permissionService.queryRoleId(userrole.getId());
            permissions.forEach(permission ->{
                //角色下面的权限
                if (StringUtils.isNotEmpty(permission.getPerms())){
                    authorizationInfo.addStringPermission(permission.getPerms());
                }
            });
        });

        return authorizationInfo;
    }

    @Override
    protected void assertCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) throws AuthenticationException {
        //不需要验证
    }

    /**
     * 清理缓存权限
     */
    public void clearCachedAuthorizationInfo() {
        this.clearCachedAuthorizationInfo(SecurityUtils.getSubject().getPrincipals());
    }
}
