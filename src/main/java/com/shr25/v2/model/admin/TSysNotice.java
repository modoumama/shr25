
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableName;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import lombok.Data;

/**
 * 公告对象 t_sys_notice
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_notice")
public class TSysNotice extends BaseDateTimeEntity
{
    /** 标题 */
    private String title;

    /** 内容 */
    private String content;

    /** 类型 */
    private Integer type;
}
