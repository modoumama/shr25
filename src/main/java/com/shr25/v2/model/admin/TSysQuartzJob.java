
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import lombok.Data;

/**
 * 定时任务调度对象 t_sys_quartz_job
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_quartz_job")
public class TSysQuartzJob extends BaseDateTimeEntity
{
    /** 任务名称 */
    private String jobName;

    /** 任务组名 */
    private String jobGroup;

    /** 调用目标字符串 */
    private String invokeTarget;

    /** cron执行表达式 */
    private String cronExpression;

    /** cron计划策略 */
    private String misfirePolicy;

    /** 是否并发执行（0允许 1禁止） */
    private String concurrent;

    /** 任务状态（0正常 1暂停） */
    private Integer status;

    /** 手动执行人 */
    @TableField(exist = false)
    private TSysUser user;
}
