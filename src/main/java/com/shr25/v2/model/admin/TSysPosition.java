
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableName;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * 岗位对象 t_sys_position
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_position")
@ApiModel(value = "SysPosition", description = "岗位表")
public class TSysPosition extends BaseDateTimeEntity
{
    /** 岗位名称 */
    @ApiModelProperty(value = "岗位名称")
    private String postName;

    /** 排序 */
    @ApiModelProperty(value = "排序")
    private Integer orderNum;

    /** 状态 */
    @ApiModelProperty(value = "状态")
    private Integer status;
}
