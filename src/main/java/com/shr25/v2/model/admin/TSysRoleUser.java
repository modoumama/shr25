
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import lombok.Data;
/**
 * 用户角色中间对象 t_sys_role_user
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_role_user")
public class TSysRoleUser extends BaseDateTimeEntity
{
    /** 用户id */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long sysUserId;

    /** 角色id */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long sysRoleId;
}
