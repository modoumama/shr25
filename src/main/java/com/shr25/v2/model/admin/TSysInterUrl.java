
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableName;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 拦截url对象 t_sys_inter_url
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_inter_url")
@ApiModel(value="TSysInterUrl", description="拦截url表")
public class TSysInterUrl extends BaseDateTimeEntity
{
    /** 拦截名称 */
    @ApiModelProperty(value = "拦截名称")
    private String interName;

    /** 拦截url */
    @ApiModelProperty(value = "拦截url")
    private String url;

    /** 类型 */
    @ApiModelProperty(value = "类型")
    private Integer type;
}
