
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.shr25.v2.common.base.BaseCreateTimeEntity;
import lombok.Data;

import java.util.Date;

/**
 * 定时任务调度日志对象 t_sys_quartz_job_log
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_quartz_job_log")
public class TSysQuartzJobLog extends BaseCreateTimeEntity
{
    /** 任务名称 */
    private String jobName;

    /** 任务组名 */
    private String jobGroup;

    /** 调用目标字符串 */
    private String invokeTarget;

    /** 日志信息 */
    private String jobMessage;

    /** 执行状态（0正常 1失败） */
    private Integer status;

    /** 异常信息 */
    private String exceptionInfo;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date endTime;
}
