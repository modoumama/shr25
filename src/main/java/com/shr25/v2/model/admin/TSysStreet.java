
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableName;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import lombok.Data;
/**
 * 街道设置对象 t_sys_street
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_street")
public class TSysStreet extends BaseDateTimeEntity
{
    /** 街道代码 */
    private String streetCode;

    /** 父级区代码 */
    private String areaCode;

    /** 街道名称 */
    private String streetName;

    /** 简称 */
    private String shortName;

    /** 经度 */
    private String lng;

    /** 纬度 */
    private String lat;

    /** 排序 */
    private Integer sort;

    /** 状态 */
    private Integer dataState;
}
