
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableName;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import lombok.Data;

/**
 * 字典类型对象 t_sys_dict_type
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_dict_type")
public class TSysDictType extends BaseDateTimeEntity
{
    /** 字典名称 */
    private String dictName;

    /** 字典类型 */
    private String dictType;

    /** 状态（0正常 1停用） */
    private String status;

    /** 备注 */
    private String remark;
}
