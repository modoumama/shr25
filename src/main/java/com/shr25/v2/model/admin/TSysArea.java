
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableName;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import lombok.Data;
/**
 * 地区设置对象 t_sys_area
 * 
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_area")
public class TSysArea extends BaseDateTimeEntity
{
    /** 区代码 */
    private String areaCode;

    /** 父级市代码 */
    private String cityCode;

    /** 市名称 */
    private String areaName;

    /** 简称 */
    private String shortName;

    /** 经度 */
    private String lng;

    /** 纬度 */
    private String lat;

    /** 排序 */
    private Integer sort;

    /** 状态 */
    private Integer dataState;
}
