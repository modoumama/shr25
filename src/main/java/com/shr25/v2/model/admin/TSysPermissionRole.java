
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import lombok.Data;
/**
 * 角色权限中间对象 t_sys_permission_role
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_permission_role")
public class TSysPermissionRole extends BaseDateTimeEntity
{
    /** 角色id */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long roleId;

    /** 权限id */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long permissionId;
}
