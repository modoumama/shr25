
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableName;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import lombok.Data;

/**
 * 公告_用户外键对象 t_sys_notice_user
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_notice_user")
public class TSysNoticeUser extends BaseDateTimeEntity
{
    /** 公告id */
    private Long noticeId;

    /** 用户id */
    private Long userId;

    /** 0未阅读 1 阅读 */
    private Integer state;
}
