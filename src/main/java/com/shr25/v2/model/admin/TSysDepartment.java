
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * 部门对象 t_sys_department
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_department")
@ApiModel(value = "SysDepartment", description = "部门表")
public class TSysDepartment extends BaseDateTimeEntity
{
    /** 父id */
    @ApiModelProperty(value = "父id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    /** 部门名称 */
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    /** 部门负责人 */
    @ApiModelProperty(value = "部门负责人")
    private String leader;

    /** 电话 */
    @ApiModelProperty(value = "电话")
    private String phone;

    /** 邮箱 */
    @ApiModelProperty(value = "邮箱")
    private String email;

    /** 状态 */
    @ApiModelProperty(value = "状态")
    private Integer status;

    /** 排序 */
    @ApiModelProperty(value = "排序")
    private Integer orderNum;

    @TableField(exist = false)
    private Integer childCount;
}
