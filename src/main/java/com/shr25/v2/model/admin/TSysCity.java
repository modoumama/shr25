
package com.shr25.v2.model.admin;

import com.baomidou.mybatisplus.annotation.TableName;
import com.shr25.v2.common.base.BaseDateTimeEntity;
import lombok.Data;
/**
 * 城市设置对象 t_sys_city
 *
 * @author huobing
 * @date 2022-03-10
 */
@Data
@TableName("t_sys_city")
public class TSysCity extends BaseDateTimeEntity
{
    /** 市代码 */
    private String cityCode;

    /** 市名称 */
    private String cityName;

    /** 简称 */
    private String shortName;

    /** 省代码 */
    private String provinceCode;

    /** 经度 */
    private String lng;

    /** 纬度 */
    private String lat;

    /** 排序 */
    private Integer sort;

    /** 状态 */
    private Integer dataState;
}
