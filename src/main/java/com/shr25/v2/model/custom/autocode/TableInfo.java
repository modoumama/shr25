package com.shr25.v2.model.custom.autocode;

import com.shr25.v2.util.StringUtils;
import org.apache.commons.lang.WordUtils;

import java.util.List;

/**
 * 数据库表对象
 */
public class TableInfo {

    /**
     * 数据库表名字 t_fifle
     */
    private String tableName;

    /**
     * java表名字例如    SysOperLog
     */
    private String javaTableName;

    /**
     * 数据表注释 例如文件管理系统
     */
    private String tableComment;

    /**
     * java表名字例如    sysOperLog
     */
    private String javaTableName_a;

    /** 是否有时间类型参数 */
    private boolean isdate = false;

    /** 是否有Long类型参数 */
    private boolean islong = false;

    /** java表名字例如    sysOperLog */
    private String baseEntityName = "BaseEntity";

    /**
     * 字段集合
     */
    List<BeanColumn> beanColumns;

    /**
     * Entity基类字段
     */
    public static final String[] BASE_ENTITY = {"id","createId", "createBy", "createTime", "updateId", "updateBy", "updateTime"};

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getJavaTableName() {
        return javaTableName;
    }

    public void setJavaTableName(String javaTableName) {
        this.javaTableName = javaTableName;
    }

    public List<BeanColumn> getBeanColumns() {
        return beanColumns;
    }

    public void setBeanColumns(List<BeanColumn> beanColumns) {
        this.beanColumns = beanColumns;
    }

    public String getTableComment() {
        return tableComment;
    }

    public void setTableComment(String tableComment) {
        this.tableComment = tableComment;
    }

    public String getJavaTableName_a() {
        return javaTableName_a;
    }

    public void setJavaTableName_a(String javaTableName_a) {
        this.javaTableName_a = javaTableName_a;
    }

    public boolean isIsdate() {
        return isdate;
    }

    public void setIsdate(boolean isdate) {
        this.isdate = isdate;
    }

    public boolean isIslong() {
        return islong;
    }

    public void setIslong(boolean islong) {
        this.islong = islong;
    }

    public String getBaseEntityName() {
        return baseEntityName;
    }

    public void setBaseEntityName(String baseEntityName) {
        this.baseEntityName = baseEntityName;
    }

    public TableInfo(String tableName, List<BeanColumn> beanColumns, String tableComment, String tablePrefix) {
        super();
        this.tableName = tableName;
        this.javaTableName = tableToJava(tableName, tablePrefix);
        this.beanColumns = beanColumns;
        for (BeanColumn beanColumn: beanColumns) {
            if(isSuperColumn(beanColumn.getBeanName())){
                if(beanColumn.getColumn_name().equals("update_id") && !baseEntityName.equals("BaseDateTimeEntity")){
                    baseEntityName = "BaseDateTimeEntity";
                }else if(beanColumn.getColumn_name().equals("create_id") && baseEntityName.equals("BaseEntity")){
                    baseEntityName = "BaseCreateTimeEntity";
                }
            }else{
                if(beanColumn.getData_type().equals("datetime")){
                    isdate = true;
                }
                if(beanColumn.getData_type().equals("bigint")){
                    islong = true;
                }
            }

        }
        this.tableComment = tableComment;
        this.javaTableName_a = tableToJava_a(tableName, tablePrefix);
    }

    public TableInfo() {
        super();
    }

    /**
     * 列名转换成Java属性名
     *
     * @param columnName
     * @return
     */
    public String columnToJava(String columnName) {
        return WordUtils.capitalizeFully(columnName, new char[]{'_'}).replace("_" , "");
    }

    /**
     * 表名转换成Java类名
     *
     * @param tableName
     * @param tablePrefix
     * @return
     */
    public String tableToJava(String tableName, String tablePrefix) {
        //String tablePrefix = Objects.requireNonNull(AutoCodeConfig.getConfig()).getString("tablePrefix");
        if (StringUtils.isNotBlank(tablePrefix)) {
            tableName = tableName.replaceFirst(tablePrefix, "");
        }
        return columnToJava(tableName);
    }

    /**
     * 首字母小写
     *
     * @param tableName
     * @param tablePrefix
     * @return
     */
    public String tableToJava_a(String tableName, String tablePrefix) {
        String str = tableToJava(tableName, tablePrefix);
        return StringUtils.firstLowerCase(str);
    }

    /**
     *  是否是继承的属性
     *
     * @param javaField
     * @return
     */
    public static boolean isSuperColumn(String javaField) {
        return StringUtils.equalsAnyIgnoreCase(javaField, BASE_ENTITY);
    }

    public static void main(String[] args) {
        System.out.println(isSuperColumn("createId"));
    }
}
