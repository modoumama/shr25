package com.shr25.v2.model.custom.autocode;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class AutoCodeConfig {
    private static PropertiesConfiguration proCfing;
    static {
        try {
            proCfing = new PropertiesConfiguration();
            proCfing.setEncoding("UTF-8");
            proCfing.load("auto_code/auto_code_config.properties");
        } catch (ConfigurationException e) {
            System.out.println("获取配置文件失败");
            e.printStackTrace();
        }
    }
    
    /**
     * 获取配置文件value
     * @param key
     * @return
     * @author fuce
     * @Date 2019年8月17日 上午12:29:40
     */
    public static  String getProperty(String key) {
    	return proCfing.getString(key);
    }
   
    
    
}
