
package com.shr25.v2.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shr25.v2.model.admin.TSysQuartzJob;

/**
 * 定时任务调度Mapper接口
 * 
 * @author zhaonz
 * @date 2021-08-06
 */
public interface TSysQuartzJobMapper extends BaseMapper<TSysQuartzJob> {

}
