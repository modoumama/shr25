
package com.shr25.v2.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shr25.v2.model.admin.TSysOperLog;

/**
 * 日志记录Mapper接口
 * 
 * @author zhaonz
 * @date 2021-08-06
 */
public interface TSysOperLogMapper extends BaseMapper<TSysOperLog> {

    /**
     * 清空日志
     *
     * @return
     */
    public void cleanTSysOperLog();
}
