
package com.shr25.v2.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shr25.v2.model.admin.TSysProvince;

/**
 * 省份Mapper接口
 * 
 * @author zhaonz
 * @date 2021-08-05
 */
public interface TSysProvinceMapper extends BaseMapper<TSysProvince>
{

}
