
package com.shr25.v2.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shr25.v2.model.admin.TSysQuartzJobLog;

/**
 * 定时任务调度日志Mapper接口
 * 
 * @author zhaonz
 * @date 2021-08-06
 */
public interface TSysQuartzJobLogMapper extends BaseMapper<TSysQuartzJobLog> {

    /**
     * 清空定时任务调度日志信息
     */
    public void cleanQuartzJobLog();
}
