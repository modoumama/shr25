
package com.shr25.v2.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shr25.v2.model.admin.TSysInterUrl;

/**
 * 拦截urlMapper接口
 * 
 * @author zhaonz
 * @date 2021-08-06
 */
public interface TSysInterUrlMapper extends BaseMapper<TSysInterUrl> {

}
