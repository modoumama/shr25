package com.shr25.v2.common.file;

import lombok.Data;

/**
 * 文件处理工具类
 * @author huobing
 * @date: 2022年3月24日 下午10:33:31
 */
@Data
public class LocalFile {
  private String fileUrl;

  private String fileName;

  private Long fileSize;

  private String fileSuffix;
}
