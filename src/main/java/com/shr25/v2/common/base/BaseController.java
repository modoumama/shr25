package com.shr25.v2.common.base;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.shr25.v2.common.conf.V2Config;
import com.shr25.v2.common.conf.redis.RedisService;
import com.shr25.v2.common.domain.AjaxResult;
import com.shr25.v2.common.domain.ResuTree;
import com.shr25.v2.common.domain.ResultTable;
import com.shr25.v2.model.admin.TSysUser;
import com.shr25.v2.model.custom.Tablepar;
import com.shr25.v2.qq.QQUserInfo;
import com.shr25.v2.service.admin.*;
import com.shr25.v2.shiro.util.ShiroUtils;
import com.shr25.v2.util.ServletUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * web层通用数据处理
 *
 * @author fuce
 * @ClassName: BaseController
 * @date 2018年8月18日
 */
@Controller
public class BaseController {
    Logger log = LoggerFactory.getLogger(getClass());

    /**
     * 系统用户
     */
    @Autowired
    public ITSysUserService sysUserService;

    /**
     * 系统角色
     */
    @Autowired
    public ITSysRoleService sysRoleService;

    /**
     * 权限
     */
    @Autowired
    public ITSysPermissionService sysPermissionService;

    /**
     * 日志操作
     */
    @Autowired
    public ITSysOperLogService sysOperLogService;

    /**
     * 公告
     */
    @Autowired
    public ITSysNoticeService sysNoticeService;

    /**
     * 文件上传
     */
    @Autowired
    public ITSysFileService sysFileService;

    /**
     * 配置文件
     */
    @Autowired
    public V2Config v2Config;

    @Autowired
    public RedisService redisService;

    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     *
     * @param binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    /**
     * 响应返回结果
     *
     * @param rows 影响行数
     * @return 操作结果
     */
    protected AjaxResult toAjax(int rows) {
        return rows > 0 ? success() : error();
    }

    /**
     * 响应返回结果
     *
     * @param flag 成功失败
     * @return 操作结果
     */
    protected AjaxResult toAjax(Boolean flag) {
        return flag ? success() : error();
    }

    /**
     * 返回成功
     *
     * @return
     */
    public AjaxResult success() {
        return AjaxResult.success();
    }

    /**
     * 返回失败消息
     *
     * @return
     */
    public AjaxResult error() {
        return AjaxResult.error();
    }

    /**
     * 返回成功消息
     *
     * @param message
     * @return
     */
    public AjaxResult success(String message) {
        return AjaxResult.success(message);
    }

    /**
     * 返回成功消息
     *
     * @param data
     * @return
     */
    public AjaxResult successData(Object data) {
        return AjaxResult.successData(data);
    }

    /**
     * 返回object数据
     *
     * @param code
     * @param data
     * @return
     */
    public AjaxResult success(int code, String message, Object data) {
        return AjaxResult.success(code, message, data);
    }

    /**
     * 返回失败消息
     *
     * @param message
     * @return
     */
    public AjaxResult error(String message) {
        return AjaxResult.error(message);
    }

    /**
     * 返回错误码消息
     *
     * @param code
     * @param message
     * @return
     */
    public AjaxResult error(int code, String message) {
        return AjaxResult.error(code, message);
    }

    /**
     * 返回object数据
     *
     * @param code
     * @param data
     * @return
     */
    public AjaxResult retobject(int code, Object data) {
        return AjaxResult.successData(code, data);
    }

    /**
     * 页面跳转
     *
     * @param url
     * @return
     */
    public String redirect(String url) {
        return StrUtil.format("redirect:{}", url);
    }

    /**
     * 设置请求分页数据
     */
    protected void startPage() {
        Tablepar tablepar = Tablepar.buildPageRequest();

        int pageNum = tablepar.getPage();
        int pageSize = tablepar.getLimit();
        if (pageNum != 0 && pageSize != 0) {
            PageHelper.startPage(pageNum, pageSize, tablepar.getOrderByColumn());
        }
    }

    /**
     * 返回 Tree 数据
     *
     * @param data
     * @return
     */
    protected static ResuTree dataTree(Object data) {
        ResuTree resuTree = new ResuTree();
        resuTree.setData(data);
        return resuTree;
    }

    /**
     * 返回数据表格数据
     *
     * @param data  表格分页数据
     * @param count
     * @return
     */
    protected static ResultTable pageTable(Object data, long count) {
        return ResultTable.pageTable(count, data);
    }

    /**
     * 返回数据表格数据
     *
     * @param data 表格分页数据
     * @return
     */
    protected static ResultTable dataTable(Object data) {
        return ResultTable.dataTable(data);
    }

    /**
     * 返回树状表格数据
     *
     * @param data 表格分页数据
     * @return
     */
    protected static ResultTable treeTable(Object data) {
        return ResultTable.dataTable(data);
    }

    /**
     * 返回qq登录
     * @return
     */
    protected ModelAndView loginbyqq(){
        redisService.setCacheObject(ShiroUtils.getSessionId(), ServletUtils.getRequest().getRequestURL(), 60L, TimeUnit.SECONDS);
        return redirectView("/loginbyqq");
    }

    /**
     * 获取qq用户信息
     * @return
     */
    protected QQUserInfo getQQUserInfo(){
        QQUserInfo qqUserInfo = (QQUserInfo) ServletUtils.getSession().getAttribute("qqUserInfo");
        if(qqUserInfo != null){
            log.info("qqUserInfo:{}", JSON.toJSONString(qqUserInfo));
        }else{
            TSysUser sysUser = ShiroUtils.getUser();
            if(sysUser!=null && sysUser.getQqOpenid() != null){
                qqUserInfo = new QQUserInfo();
                qqUserInfo.setOpenid(sysUser.getQqOpenid());
                qqUserInfo.setNickname(sysUser.getNickname());
                qqUserInfo.setFigureurl_qq(sysUser.getQqFigureurl());
                ServletUtils.getSession().setAttribute("qqUserInfo", qqUserInfo);
            }
        }
        return qqUserInfo;
    }

    /**
     * redirect重定向
     * @param url
     * @return
     */
    public ModelAndView modelAndView(String url){
        return new ModelAndView(url);
    }

    /**
     * redirect重定向
     * @param url
     * @return
     */
    public ModelAndView redirectView(String url){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setView(new RedirectView(url, true, false));
        return modelAndView;
    }
}
