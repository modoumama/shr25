package com.shr25.v2.common.base;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 公共service
 *
 * @author huobing
 * @date 2022-03-13 10:41:19
*/
public interface BaseService<T> extends IService<T>{

    /**
     * 批量删除
     * @param ids
     * @return
     */
    Boolean removeByIds(String ids);
}
