package com.shr25.v2.common.commons;

/**
 * @创建人 huobing
 * @创建时间 2022-03-13 09:57
 * @功能描述 评审类型
 **/
public class FileType {
    /** TF.任务附件 */
    public static final String TF = "TF";

    /** TEF执行凭据 */
    public static final String TEF = "TEF";
}
