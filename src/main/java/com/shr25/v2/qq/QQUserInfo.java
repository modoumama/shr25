package com.shr25.v2.qq;

import lombok.Data;

import java.io.Serializable;

/**
 * qq用户信息
 *
 * @author huobing
 * @date 2022年04月12日
 */
@Data
public class QQUserInfo implements Serializable {
  public static final long serialVersionUID = 1L;
  private String openid;
  private String nickname;
  private String gender;
  private String figureurl;
  private String figureurl_qq;
}
