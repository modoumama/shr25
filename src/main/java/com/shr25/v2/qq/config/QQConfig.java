package com.shr25.v2.qq.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * qq配置
 *
 * @author huobing
 * @date 2022年04月12日
 */
@Data
@Component
@ConfigurationProperties(prefix = "qq")
public class QQConfig
{
	/** 文件路径 **/
	private String appid;
	/** 文件路径 **/
	private String appkey;

	private String redirectUri;

	private String fmt = "json";
}
