package com.shr25.v2.qq;

import lombok.Data;

/**
 * qq应用AccessToken
 *
 * @author huobing
 * @date 2022年04月12日
 */
@Data
public class AccessToken {
  /** 授权令牌 */
  String access_token;

  /** expires_in */
  Long expires_in;

  /** refresh_token */
  String refresh_token;
}
